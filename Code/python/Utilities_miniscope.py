# Imports
import numpy as np
from matplotlib import pyplot as plt


def idx2range(idx):
    # Convert to numpy array
    if not type(idx) is np.ndarray:
        idx = np.array([idx], dtype=int).ravel()

    if idx.shape[0] > 1:
        # Find discontinuities in index
        dataIDX = np.atleast_2d(np.unique(np.hstack((0, np.where(np.diff(idx) > 1)[0]+1)))).transpose()
        dataIDX = np.hstack((dataIDX, np.atleast_2d(np.hstack((dataIDX[1:,0]-1, idx.shape[0]-1))).transpose()))
        # Get original values
        dataIDX = idx[dataIDX]

        # Add column for duration
        dataIDX = np.hstack((dataIDX, np.atleast_2d(dataIDX[:, 1] - dataIDX[:, 0] + 1).transpose()))

    else:
        dataIDX = np.empty((0, 3), dtype=int)

    return dataIDX


def rotate(origin, points, angle):
    """
    Rotate a point clockwise by a given angle around a given origin.

    The angle should be given in degrees.
    """
    ox, oy = origin
    angle_rad = angle * np.pi / 180

    qx = ox + np.cos(angle_rad) * (points[:, 0] - ox) - np.sin(angle_rad) * (points[:, 1] - oy)
    qy = oy + np.sin(angle_rad) * (points[:, 0] - ox) + np.cos(angle_rad) * (points[:, 1] - oy)
    return qx, qy


def segment_epm_maze(x, y, open_arms):
    """ 
    left_arm = np.where((x <= -13) & (x >= -50) & (y <= -1) & (y >= -5))[0]
    right_arm = np.where((x <= 50) & (x >= -6) & (y <= -1) & (y >= -5))[0]
    top_arm = np.where((x <= -6) & (x >= -13) & (y <= 50) & (y >= 1))[0]
    bottom_arm = np.where((x <= -6) & (x >= -13) & (y <= -5) & (y >= -50))[0]
    center_arm = np.where((x <= -6) & (x > -13) & (y <= -1) & (y >= -5))[0]
    """
    # segment manually
    plt.figure()
    plt.plot(x, y, 'k')
    plt.title("Segment the EPM maze")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    #plt.show()
    # ask usier input to draw a rectangle
    print("Draw a rectangle around the open arms")
    arms= plt.ginput(2)
    # close figure
    # get the coordinates of the rectangle
    t = arms[0][1]
    b = arms[1][1]
    l = arms[0][0]
    r = arms[1][0]
    """ 
    t = arms[0][0]
    b = arms[1][0]
    l = arms[0][1]
    r = arms[1][1]
 """
    plt.close()

    # plot a squre on the plot to check if the rectangle is correct
    plt.figure()
    plt.plot(x, y, 'k')
    plt.title("Segment the EPM maze")
    plt.xlabel("X position")
    plt.ylabel("Y position")
    plt.plot([l, r], [t, t], 'r')
    plt.plot([l, r], [b, b], 'r')
    plt.plot([l, l], [t, b], 'r')
    plt.plot([r, r], [t, b], 'r')
    plt.show()

    left_arm = np.where((x <= l) & (x >= -50) & (y <= t) & (y >= b))[0]
    right_arm = np.where((x <= 50) & (x >= r) & (y <= t) & (y >= b))[0]
    top_arm = np.where((x <= r) & (x >= l) & (y <= 50) & (y >= t))[0]
    bottom_arm = np.where((x <= r) & (x >= l) & (y <= b) & (y >= -50))[0]
    center_arm = np.where((x <= r) & (x >= l) & (y <= t) & (y >= b))[0]
    if False:
        # check if the segmentation is correct by plotting the center arm
        plt.figure()
        plt.plot(x, y, 'k')
        plt.title("Segment the EPM maze")
        plt.xlabel("X position")
        plt.ylabel("Y position")
        plt.plot(x[center_arm], y[center_arm], 'r')
        plt.show()

    if open_arms == 'top_bottom':
        open_arms_pos = [top_arm, bottom_arm]
        closed_arms_pos = [left_arm, right_arm]
    elif open_arms == 'left_right':
        open_arms_pos = [left_arm, right_arm]
        closed_arms_pos = [top_arm, bottom_arm]
    return open_arms_pos, closed_arms_pos, center_arm
