import cv2
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from PIL import Image, ImageTk


def browse_video():
    global input_video
    file_path = filedialog.askopenfilename()
    input_video = cv2.VideoCapture(file_path)
    update_frame()


def update_frame():
    global input_video
    ret, frame = input_video.read()

    if not ret:
        input_video.set(cv2.CAP_PROP_POS_FRAMES, 0)
        ret, frame = input_video.read()

    frame = cv2.resize(frame, (600, 400))
    frame = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA))
    frame = ImageTk.PhotoImage(image=frame)

    video_label.config(image=frame)
    video_label.image = frame
    video_label.after(10, update_frame)


def save_adjusted_video():
    global input_video
    file_path = filedialog.asksaveasfilename(defaultextension=".avi")
    if not file_path:
        return

    codec = int(input_video.get(cv2.CAP_PROP_FOURCC))
    fps = int(input_video.get(cv2.CAP_PROP_FPS))
    frame_size = (int(input_video.get(cv2.CAP_PROP_FRAME_WIDTH)), int(input_video.get(cv2.CAP_PROP_FRAME_HEIGHT)))

    output_video = cv2.VideoWriter(file_path, codec, fps, frame_size)

    input_video.set(cv2.CAP_PROP_POS_FRAMES, 0)

    while True:
        ret, frame = input_video.read()
        if not ret:
            break

        frame = cv2.convertScaleAbs(frame, alpha=contrast_scale.get(), beta=brightness_scale.get())

        output_video.write(frame)

    output_video.release()
    messagebox.showinfo("Success", "Video adjustments saved successfully")


root = tk.Tk()
root.title("Video Adjuster")

frame = tk.Frame(root)
frame.pack(padx=10, pady=10)

video_label = tk.Label(frame)
video_label.grid(row=0, column=0, columnspan=2)

browse_button = tk.Button(frame, text="Browse Video", command=browse_video)
browse_button.grid(row=1, column=0, pady=5)

save_button = tk.Button(frame, text="Save Adjusted Video", command=save_adjusted_video)
save_button.grid(row=1, column=1, pady=5)

contrast_label = tk.Label(frame, text="Contrast:")
contrast_label.grid(row=2, column=0)

brightness_label = tk.Label(frame, text="Brightness:")
brightness_label.grid(row=3, column=0)

contrast_scale = tk.Scale(frame, from_=0.5, to=3.0, resolution=0.1, orient=tk.HORIZONTAL)
contrast_scale.set(1.0)
contrast_scale.grid(row=2, column=1)

brightness_scale = tk.Scale(frame, from_=-100, to=100, resolution=1, orient=tk.HORIZONTAL)
brightness_scale.set(0)
brightness_scale.grid(row=3, column=1)

input_video = None
root.mainloop()
