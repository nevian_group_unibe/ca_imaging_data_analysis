import cv2
import os
import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox

class VideoTrimmer:
    def __init__(self, master):
        self.master = master
        self.master.title("Video Trimmer and Frame Reader")

        self.file_path = tk.StringVar()

        self.load_button = tk.Button(self.master, text="Load Video", command=self.load_video)
        self.load_button.pack()

        self.trim_button = tk.Button(self.master, text="Trim Video", command=self.trim_video, state=tk.DISABLED)
        self.trim_button.pack()

        self.read_frame_button = tk.Button(self.master, text="Read Frame", command=self.read_frame, state=tk.DISABLED)
        self.read_frame_button.pack()

    def load_video(self):
        file_path = filedialog.askopenfilename(filetypes=[("Video files", "*.mp4;*.avi;*.mkv")])
        if file_path:
            self.file_path.set(file_path)
            self.trim_button.config(state=tk.NORMAL)
            self.read_frame_button.config(state=tk.NORMAL)

    def trim_video(self):
        if self.file_path.get():
            start_time = float(input("Enter the start time (in seconds): "))
            end_time = float(input("Enter the end time (in seconds): "))

            input_video = cv2.VideoCapture(self.file_path.get())
            fps = int(input_video.get(cv2.CAP_PROP_FPS))
            fourcc = int(input_video.get(cv2.CAP_PROP_FOURCC))

            input_directory, input_filename = os.path.split(self.file_path.get())
            input_filename_no_ext = os.path.splitext(input_filename)[0]
            output_file_path = os.path.join(input_directory, input_filename_no_ext + '_trim' + os.path.splitext(input_filename)[1])

            output_video = cv2.VideoWriter(output_file_path, fourcc, fps, (int(input_video.get(3)), int(input_video.get(4))))

            start_frame = int(start_time * fps)
            end_frame = int(end_time * fps)

            current_frame = 0
            while input_video.isOpened():
                ret, frame = input_video.read()
                if not ret:
                    break

                if start_frame <= current_frame <= end_frame:
                    output_video.write(frame)

                current_frame += 1
                if current_frame > end_frame:
                    break

            input_video.release()
            output_video.release()
            messagebox.showinfo("Video Trimmer", f"Trimmed video saved as {output_file_path}")

    def read_frame(self):
        if self.file_path.get():
            frame_number = int(input("Enter the frame number: "))
            input_video = cv2.VideoCapture(self.file_path.get())
            input_video.set(cv2.CAP_PROP_POS_FRAMES, frame_number)
            ret, frame = input_video.read()
            if ret:
                cv2.imshow(f"Frame {frame_number}", frame)
                cv2.waitKey(0)
                cv2.destroyAllWindows()
            else:
                messagebox.showerror("Error", f"Could not read frame {frame_number}")

            input_video.release()

if __name__ == "__main__":
    root = tk.Tk()
    app = VideoTrimmer(root)
    root.mainloop()
