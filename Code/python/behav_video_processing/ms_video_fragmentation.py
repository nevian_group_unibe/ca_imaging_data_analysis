import cv2
import numpy as np
import os
import pandas as pd
from pathlib import Path

def get_user_animal_ids():
    """Get animal IDs from user input"""
    print("\nEnter animal ID(s) in any of these formats:")
    print("- Single animal: 1378")
    print("- Multiple animals separated by comma: 1378,1380,1383")
    print("- Press Enter for all animals")
    
    user_input = input("\nEnter animal ID(s): ").strip()
    
    if not user_input:
        # Default list of all animals
        return ['ID_1378', 'ID_1380', 'ID_1383', 'ID_1384', 'ID_1386', 
                'ID_1387', 'ID_1388', 'ID_1389', 'ID_1390', 'ID_1391', 'ID_1392']
    
    # Process user input
    animal_ids = [f"ID_{id.strip()}" for id in user_input.split(',')]
    return animal_ids

def get_animal_sessions(metadata_path, animal_id):
    """Read metadata from CSV file for a specific animal and return available sessions"""
    csv_file = os.path.join(metadata_path, f"{animal_id}.csv")
    if not os.path.exists(csv_file):
        return []
    
    sessions = []
    try:
        df = pd.read_csv(csv_file)
        # Convert dates to strings and ensure 6-digit format
        sessions = [str(date).zfill(6) for date in df['date'].unique()]
    except Exception as e:
        print(f"Error reading metadata file {csv_file}: {str(e)}")
    
    return sorted(sessions)

def process_video_file(video_path, output_folder, frames_per_chunk=1000):
    """Process a single video file into chunks"""
    cap = cv2.VideoCapture(str(video_path))
    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    frame_rate = cap.get(cv2.CAP_PROP_FPS)
    num_chunks = (total_frames + frames_per_chunk - 1) // frames_per_chunk

    for chunk_idx in range(num_chunks):
        chunk_frames = []
        for _ in range(frames_per_chunk):
            ret, frame = cap.read()
            if not ret:
                break
            chunk_frames.append(frame)

        if chunk_frames:
            output_file_name = f'msCam{chunk_idx+1}.avi'
            height, width, layers = chunk_frames[0].shape
            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            output_path = os.path.join(output_folder , output_file_name)
            out = cv2.VideoWriter(str(output_path), fourcc, frame_rate, (width, height))
            
            for frame in chunk_frames:
                out.write(frame)
                # development status bar
                print(f"Processing {output_file_name}: {cap.get(cv2.CAP_PROP_POS_FRAMES)}/{total_frames}", end='\r')
                
            out.release()
            chunk_frames.clear()
    print(f'Processed frames: {total_frames}')
    cap.release()
    return num_chunks

def main():
    # Define paths using raw strings and os.path.join for cross-platform compatibility
    metadata_base_path = os.path.join(r'V:', 'Ca_imaging_pain', '0_movie_metadata')
    video_base_path = os.path.join(r'H:', 'Miniscope_data', 'ACC_pain_kinematics')
    
    # Convert paths to Path objects for later operations
    metadata_base_path = Path(metadata_base_path)
    video_base_path = Path(video_base_path)
    
    # Get animal IDs from user
    animal_ids = get_user_animal_ids()
    print(f"\nProcessing the following animals: {', '.join(animal_ids)}")
    
    # Ask for frames per chunk
    try:
        frames_per_chunk = int(input("\nEnter frames per chunk (default: 1000): ") or 1000)
    except ValueError:
        frames_per_chunk = 1000
        print("Invalid input, using default value: 1000 frames per chunk")

    # Process each animal
    for animal_id in animal_ids:
        print(f"\nProcessing animal: {animal_id}")
        
        # Get sessions for this animal
        sessions = get_animal_sessions(metadata_base_path, animal_id)
        
        if not sessions:
            print(f"No sessions found for {animal_id}")
            continue
            
        print(f"Found {len(sessions)} sessions")
        
        for session in sessions:
            # Convert all path components to strings and use os.path.join
            video_path = os.path.join(str(video_base_path), str(animal_id), str(session), 'msCam0.avi')
            if not os.path.exists(video_path):
                print(f"Video not found for {animal_id}/{session}")
                continue

            print(f"Processing session: {session}")
            output_folder = os.path.dirname(video_path)
            try:
                num_chunks = process_video_file(video_path, output_folder, frames_per_chunk)
                # delete the original video file
                os.remove(video_path)
                print(f"Completed {animal_id}/{session}: Created {num_chunks} chunks")
            except Exception as e:
                print(f"Error processing {animal_id}/{session}: {str(e)}")

if __name__ == "__main__":
    main()
