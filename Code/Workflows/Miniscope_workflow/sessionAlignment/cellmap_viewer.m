function [meanImage, userApproval] = cellmap_viewer(mapsAligned)
% CELLMAP_VIEWER Calculates and displays mean image from cell array of images
% Inputs:
%   mapsAligned - Cell array containing images of size 290x310
% Outputs:
%   meanImage - The calculated mean image
%   userApproval - Boolean indicating if results were approved by user

% Calculate mean image
numImages = length(mapsAligned);
meanImage = zeros(290, 310);

% Create figure with subplots
fig_sessions = figure('Position', [100 100 1200 500]);
% Sum all images
for i = 1:numImages
    subplot(1,numImages,i);
    currentImage = mapsAligned{i};
    imagesc(currentImage)
    axis square
    meanImage = meanImage + double(currentImage);
end

% Calculate mean
meanImage = meanImage / numImages;

% Plot the mean image
fig_mean = figure('Position', [1200 500 1200 500]);
imagesc(meanImage)
axis square


% Create dialog box for user input
answer = questdlg('Are the results acceptable?', ...
    'Quality Check', ...
    'Yes','No','Yes');

% Handle response
switch answer
    case 'Yes'
        userApproval = true;
    case 'No'
        userApproval = false;
    otherwise
        userApproval = false; % Default to false if dialog is closed
end

% Optional: Close figure if not needed anymore
% close(gcf);

end