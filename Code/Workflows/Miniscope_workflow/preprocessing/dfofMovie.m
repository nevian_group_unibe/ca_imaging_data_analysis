function [thisMovie] = dfofMovie(thisMovie)
% converts movie to units of DFOF
    
    % adjust for problems with movies that have negative pixel values before dfof
    minMovie = min(thisMovie(:));
    if minMovie < 0
        thisMovie = thisMovie + 1.1*abs(minMovie);
    end

    % Old implementation
    % get the movie F0, do by row to reduce potential memory errors	    
    inputMovieF0 = zeros([size(thisMovie,1) size(thisMovie,2)]);
    parfor rowNo = 1:size(thisMovie,1)
        inputMovieF0(rowNo,:) = nanmean(squeeze(thisMovie(rowNo,:,:)),2);
    end

    % bsxfun for fast matrix divide
    thisMovie_out = bsxfun(@ldivide,inputMovieF0,thisMovie);

    % % New implementation
    % thisMovie_out = zeros([size(thisMovie,1) size(thisMovie,2), size(thisMovie,3)]);
    % MEANS = zeros(size(thisMovie,3),1);
    % parfor mvN = 1:size(thisMovie,3)
    %     % this_sq  = thisMovie(1:30, 1:30, :);
    %     this_d = thisMovie(:,:,mvN);
    %     MEANS(mvN) = mean(this_d(:));
    %     thisMovie_out(:,:, mvN) = thisMovie(:,:,mvN) /  MEANS(mvN)
    % 
    % end


    thisMovie_out = thisMovie_out-1;

end


