% Let's select the animals here, just hardocded for now
global GC
GC = general_configs();
% check folder: and then take the animal_ids as animal_list
folder = 'K:\Ca_imaging_pain\2_motion_corrected_movies';

% Get all directories that match the pattern 'SS_' followed by 4 digits
dir_content = dir(fullfile(folder, 'SS_*'));
animal_list = {};
for i = 1:length(dir_content)
    if dir_content(i).isdir && ~isempty(regexp(dir_content(i).name, '^SS_\d{4}$'))
        animal_list{end+1} = dir_content(i).name;
    end
end

for iid = 1:length(animal_list)
    animal_ID = animal_list{iid};
    here_select_sq_ROI_multi_animal(animal_ID, false)
end



function here_select_sq_ROI_multi_animal(animal_ID, missing_only)

% Import global variables with parameters and for logging
global GC LOGGER

toggle_toolbox('_plotting', 'on')

%% Create parameter structure 
p_filename = get_filename_of('miniscope_movie_parameters', animal_ID);
% 
% % Get info from database
  METADATA = SQL_database.read_table_where('sessions', {}, animal_ID,'animal_ID');
 experiments = uniqueCellRows(METADATA{:, {'date', 'experiment'}}, 'rows');
 n_experiments = size(experiments, 1);
 done_preprocessing = unique(SQL_database.read_table_where('sessions', {'experimental_condition','done_preprocessing'}, animal_ID, 'animal_ID', 'split_experimental_condition', false), 'stable');
 done_preprocessing = done_preprocessing.done_preprocessing;

if exist(p_filename, 'file')
    p = load_variable(p_filename, 'p');
    
else  % Make a new structure
    p = params();
    p.nSessions = n_experiments;
    p.experiment = {''};
    p.subjects = {animal_ID};
    p.user.large_data = true;
    p.user.turboreg = struct();
    p.user.frameRate = NaN(n_experiments, 1);
end
% experiments = p.e;
% p.nSessions = n_experiments;
% p.user.experiments = experiments;
% p.PCAICA.nPCs = GC.epifluorescence_PCAICA_nPCs;
% p.PCAICA.nICs = GC.epifluorescence_PCAICA_nICs;
% save(p_filename, 'p');
% 


%% Concatenate and extract signals from big data sets
sessionDirs = cell(p.nSessions, 1);
for i_sess = 1:p.nSessions
    movPath = get_filename_of('miniscope_movie_avi', animal_ID, experiments{i_sess, 1}, experiments{i_sess, 2}, '1');
    sessionDirs{i_sess} = fileparts(movPath);
end

select_sq_ROI_multi_animal(p, sessionDirs, experiments, missing_only, done_preprocessing) 


toggle_toolbox('_plotting', 'off')


%% MLint exceptions
%#ok<*TNMLP,*VIDREAD>
end