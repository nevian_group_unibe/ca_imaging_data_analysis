function concatMovies(p, movDir, position, rawMovie_filename)

    global LOGGER
    global GC
    tic;
    is_v3 = p.user.is_v3;
    % Get names of local folder
    local_saveDir = fileparts(fileparts(rawMovie_filename));
    
    % Get name of subject and session number
    f = strsplit(rawMovie_filename, filesep());
    subject_name = f{end-3};
    session_number = f{end-2};
    % Make filename of helper file
    helper_file = os.path.join(local_saveDir, ['helper_', subject_name, '_', session_number, '.mat']);
    if exist(helper_file, 'file')
        load(helper_file, 'helper');
        do_create_files = false;
         
    else  % Made new one
        do_create_files = true;
        % count number of avi files
        fnames = dir(movDir);
        n_movies = sum(cellfun(@(x) endsWith(x, '.avi'), {fnames.name}'));

        % for Bonsai data, there are cases that only one big file cannot be
        % chunked using the python code, so we need to chunk it here 
        % (Maybe in the future, use only this one)

        if n_movies == 1
            tic
            LOGGER.info([char(datetime('now')), ' - ', subject_name ,  ' - ', session_number,' had only one file, extracting chunks now'])
            filenames = {fnames.name}';
            is_mov_idx = cellfun(@(x) endsWith(x, '.avi'), filenames);
            filename_only_movie=filenames(is_mov_idx);
            success = splitMovieChunks(filename_only_movie{1}, movDir);
            % Try ffmpeg
            if ~success
                success = splitMovieChunksFFmpeg(filename_only_movie{1}, movDir);
            end
            if ~success
                error('Splitting movies did not work')
            end

            n_movies = sum(cellfun(@(x) endsWith(x, '.avi'), {fnames.name}'));
            % re-write fname
            fnames = dir(movDir);
            tt = toc;
            LOGGER.info([char(datetime('now')), ' - ', ' Creating chunks elapsed time: ', num2str(tt), 's'])

        end

        % Go on with normal procedure
        dirnames = {fnames.name}';
        movie_names = dirnames(cellfun(@(x) endsWith(x, '.avi'), {fnames.name}'));
        movie_names = natural_sort_filenames(movie_names);
        % flag for Miniscope v3, if not then it is Miniscope v4
        % try checking if it's possible to load the movies as it is
        first_movie = movie_names{1};
        first_movie_path = fullfile(movDir, first_movie);
        try
            tempObj = VideoReader(first_movie_path);
        catch
            toggle_toolbox('ffmpeg-r8', 'on')
            ffmpegsetup
            tempObj = [];

        end
       
     
        % Get number of frames
        total_nFrames = zeros(n_movies, 1);
        for m = 1:n_movies
            % load movie
            movPath = os.path.join(movDir,  movie_names{m});
            % keyboard
            % if is_v3
            movPath = os.path.join(movDir,  movie_names{m});
            % else
            % --  modified this because of files from bonsai (Change
            % back in the future) --  num2str(m-1)
            % name_vid = ['msCam', num2str(m), '.avi'];
            % movPath = os.path.join(movDir, name_vid);
            % %disp('Converting AVI codec')
            % outfile = fullfile(local_movDir, name_vid);
            % % this might not be necessary (after setting ffmpeg it gets
            % % fixed)
            % keyboard
            % %ffmpegtranscode(movPath, outfile, 'AudioCodec', 'aac', 'VideoCodec', 'x264');
            % %movPath = outfile;
        
            try
            movieObj = VideoReader(movPath);
            catch
                keyboard % there's an issue with the file?
                 name_vid = ['msCam', num2str(m-1), '.avi']; % let's see if it reads the whole avi movie from bonsai
                movPath = os.path.join(movDir, name_vid);
            end

            total_nFrames(m) = movieObj.NumberOfFrames;
        end

        % Initialize helper
        helper = struct();
    end
    
    % Initialize or store information in helper
    if ~isfield(helper, 'n_movies'), helper.n_movies = n_movies; end
    if ~isfield(helper, 'total_nFrames'), helper.total_nFrames = total_nFrames; end
    if ~isfield(helper, 'position'), helper.position = position; end
    if ~isfield(helper, 'completed_PCAICA'), helper.completed_PCAICA = false; end
    % Unpack variables
    n_movies = helper.n_movies;
    total_nFrames = helper.total_nFrames;
    position = helper.position;

    cropped_height = length(position(2):position(2)+position(4)-1);
    cropped_width = length(position(1):position(1)+position(3)-1);

    % Add more variables
    if ~isfield(helper, 'analyzed'), helper.analyzed = repmat({''}, n_movies, 1); end
    if ~isfield(helper, 'zero_mask'), helper.zero_mask = false([cropped_height, cropped_width, n_movies]); end
    if ~isfield(helper, 'max_mask'), helper.max_mask = []; end
    if ~isfield(helper, 'first_frame_downsampled'), helper.first_frame_downsampled = 0; end
    if ~isfield(helper, 'last_preprocessed_chunk'), helper.last_preprocessed_chunk = 0; end
    if ~isfield(helper, 'last_preprocessed_chunk_mean_all'), helper.last_preprocessed_chunk_mean_all = 0; end
    if ~isfield(helper, 'cropping_coordinates'), helper.cropping_coordinates = []; end
    if ~isfield(helper, 'last_preprocessed_chunk_make_flat'), helper.last_preprocessed_chunk_make_flat = 0; end
            
    % Set beginning and end of each chunk
    movie_frame_edges = [[1; cumsum(helper.total_nFrames(1:end-1)) + 1], cumsum(helper.total_nFrames)];
    
    if p.user.large_data
        % Temporary files are stored locally
        preprocessed_movie_filename = os.path.join(local_saveDir, 'preprocessedMovie.h5');
        temporary_file = os.path.join(local_saveDir, 'temp.h5');
        temporary_flat_file = os.path.join(local_saveDir, 'temp_flat.h5');
        
        if do_create_files || ~exist(temporary_file, 'file')
            if exist(temporary_file, 'file')
                delete(temporary_file)
            end
            h5create(temporary_file, '/1', [cropped_height, cropped_width, sum(total_nFrames)], 'Datatype', 'single');
        end
    end

    % re load in case we skipe the first sessions
    fnames = dir(movDir);
    dirnames = {fnames.name}';
    movie_names = dirnames(cellfun(@(x) endsWith(x, '.avi'), {fnames.name}'));
    movie_names = natural_sort_filenames(movie_names);

    % loop through avi files and concatenate them
    for m = 1:n_movies
        % for the MiniscopeV4, we need to convert the avi to grayscale
        % load movie
        % Go on with normal procedure
        movPath = os.path.join(movDir, movie_names{m});
        % if is_v3
        %     movPath = os.path.join(movDir, ['msCam', num2str(m), '.avi']);
        % else
        %     % movDir = local_movDir;
        %     % !! Move these lines back in the future, only when bonsai was
        %     % used we use the version 3, meaning not m-1.
        %     name_vid = ['msCam',num2str(m), '.avi'];
        %     movPath = os.path.join(movDir, name_vid);
        % end

        if ismember(movPath, helper.analyzed)
            continue
        end
        
        LOGGER.info([char(datetime('now')), ' Concatenating movie ' num2str(m), '/', num2str(n_movies)])

        if is_v3
            movieObj = VideoReader(movPath);
            nFrames = movieObj.NumberOfFrames;
            thisStack = squeeze(read(movieObj));
        else
            % convert to grayscale
            movieObj = VideoReader(movPath);
            nFrames = movieObj.NumberOfFrames;
            frames = read(movieObj);
            thisStack = [];
            disp('Converting to grayscale')
            for iframe=1:nFrames
                %thisStack(:,:,:,i)=mean((frames(:,:,:,i)/255),3);
               thisStack(:,:,iframe)=repmat(rgb2gray(frames(:,:,:,iframe)),[1 1 1]);
            end


        end
        % Crop movie
        thisStack = thisStack(position(2):position(2)+position(4)-1, position(1):position(1)+position(3)-1,:);
        
        % For very large datasets, it is worth to do preprocessing of each movie
        if p.user.large_data
            LOGGER.info('\tFiltering movie: dividing by lowpass')
            movie = filterMovie(p, single(thisStack), 'lowpass', false);
            % Check whether to delete the first frame
            % check if the first 10 first frames of the movie
            if size(movie,3) >= 10
                first_10_frames = movie(:, :, 1:10);
            else
               first_10_frames = movie(:, :, :);
            end
            n_zeros = squeeze(sum(sum(first_10_frames == 0, 1), 2));
            fraction_zeros = n_zeros ./ (size(thisStack, 1) * size(thisStack, 2));
            frames_to_replace = find(fraction_zeros >= GC.epifluorescence_skip_frames_with_zeros);
            if ~isempty(frames_to_replace)
                n_frames_to_replace = length(frames_to_replace);
                first_good_frame = find(fraction_zeros < GC.epifluorescence_skip_frames_with_zeros, 1, 'first');
                movie(:, :, frames_to_replace) = repmat(movie(:, :, first_good_frame), [1, 1, n_frames_to_replace]);
                LOGGER.warn([num2str(n_frames_to_replace) ' frames have been replaced by frame #', num2str(first_good_frame), ' because had too much noise'])
            end

            LOGGER.info('\tRegistering movie')
            [movie, this_zeroMask] = registerMovie(p, movie);
            % Store mask
            helper.zero_mask(:, :, m) = this_zeroMask;
            
            LOGGER.info('\tComputing dFF')
            movie = dfofMovie(movie);
            
            LOGGER.info('\tWriting movie to disk')
            h5write(temporary_file, '/1', movie, [1, 1, movie_frame_edges(m, 1)], [cropped_height, cropped_width, nFrames]);
        end
        
        % Update helper file
        helper.analyzed{m} = movPath;
        save(helper_file, 'helper', '-v7.3')
    end
    toggle_toolbox('ffmpeg-r8', 'off')
    % Clear unused variables
    clear thisStack movie
    
    % Complete preprocessing
    if p.user.large_data
        if do_create_files || ~exist(preprocessed_movie_filename, 'file')
            LOGGER.info([char(datetime('now')), ' Creating file ''preprocessedMovie.h5'''])
            if exist(preprocessed_movie_filename, 'file')
                delete(preprocessed_movie_filename)
            end
            h5create(preprocessed_movie_filename, '/1', [cropped_height, cropped_width, Inf], 'Datatype', 'single', 'ChunkSize',[cropped_height, cropped_width, 100]);
        end
        
        % Make final zeroMask
        final_zeroMask = max(helper.zero_mask, [], 3);
        % Set chunk size and number of chunks
chunk_size = max(total_nFrames);
n_chunks = ceil(sum(total_nFrames) / chunk_size);

% Initialize working status
worked_on_a_chunk = false;

% Calculate frames needed for proper downsampling
frames_per_downsample = p.downsampleTime.factor;

for i_chunk = 1:n_chunks
    worked_on_a_chunk = true;
    LOGGER.info(['\tReading chunk ', num2str(i_chunk), '/', num2str(n_chunks)])
    
    % Calculate frame boundaries with overlap for downsampling
    first_frame = (i_chunk - 1) * chunk_size + 1;
    
    % Add overlap for downsampling only if not the last chunk
    if i_chunk < n_chunks
        last_frame = (i_chunk) * chunk_size + (frames_per_downsample - 1);
    else
        last_frame = min((i_chunk) * chunk_size, sum(total_nFrames));
    end
    
    % Read movie
    movie = loadMovie(temporary_file, first_frame, last_frame);
    
    % Downsample movie
    movie = downsampleMovie(p, movie, 'time');
    
    % Calculate correct number of frames after downsampling
    expected_frames = floor((last_frame - first_frame + 1) / frames_per_downsample);
    
    % Ensure we have the correct number of frames
    if size(movie, 3) > expected_frames
        movie = movie(:, :, 1:expected_frames);
    end
    
    % Write to disk with correct frame positioning
    nFrames = size(movie, 3);
    write_start_frame = floor((first_frame - 1) / frames_per_downsample) + 1;
    
    LOGGER.info(['\tSaving movie in frames ', num2str(write_start_frame), ...
        ' to ', num2str(write_start_frame + nFrames - 1)])
    
    % Write to file
    h5write(preprocessed_movie_filename, '/1', movie, ...
        [1, 1, write_start_frame], ...
        [size(movie, 1), size(movie, 2), nFrames]);
    
    % Update helper
    helper.first_frame_downsampled = write_start_frame + nFrames;
    helper.max_mask(:, :, i_chunk) = max(movie == 0, [], 3);
    helper.last_preprocessed_chunk = i_chunk;
    
    % Save helper
    save(helper_file, 'helper', '-v7.3')
end
        if worked_on_a_chunk
            % Log time that it took for the preprocessing
            time_end_preprocessing = toc;
            LOGGER.info(['Finished preprocessing in ' num2str(time_end_preprocessing / 60, '%.2f') ' min'])
        end
        
        % Delete unused variables
        clear movie
    end
    
    % Get number of frames after downsampling
    info = h5info(preprocessed_movie_filename);
    n_frames_downsampled = info.Datasets.Dataspace.Size(3);
    
    % Start signal extraction
    if p.user.large_data
        % Compute mean across all frames
        zeroMask = max(helper.max_mask, [], 3);
        zeroMask(zeroMask > 0) = 1;
        % [N, S, W, E] = getCropCoords(zeroMask);
        
        n_chunks = ceil(n_frames_downsampled / chunk_size);
        if helper.last_preprocessed_chunk_mean_all == 0
            helper.inputMean = NaN(n_chunks, 2);
        end

        LOGGER.info('\tComputing grand mean')
        for i_chunk = 1:n_chunks
            if helper.last_preprocessed_chunk_mean_all >= i_chunk
                continue
            end
            % Read movie
            LOGGER.info(['\t\tReading chunk ', num2str(i_chunk), '/', num2str(n_chunks)])
            first_frame = (i_chunk - 1) * chunk_size + 1;
            last_frame = (i_chunk - 1) * chunk_size + chunk_size;
            last_frame = min(last_frame, n_frames_downsampled);
            movie = loadMovie(preprocessed_movie_filename, first_frame, last_frame);

            % Crop movie
            % movie = movie(N:S, W:E, :);
            % Store sum and number of pixels in movie (to ompute mean later)
            helper.inputMean(i_chunk, :) = [nansum(movie(:)), numel(movie)];

            % Store where we are
            helper.last_preprocessed_chunk_mean_all = i_chunk;
            save(helper_file, 'helper', '-v7.3')
        end

        LOGGER.info([char(datetime('now')) ' Preparing data for signal extraction'])
        % Compute mean of each frame and store flattened version of data
        % cropped_width = length(W:E);
        % cropped_height = length(N:S);
        cropped_width = size(helper.zero_mask, 2);
        cropped_height = size(helper.zero_mask, 1);
        
        % Create file
        if do_create_files || ~exist(temporary_flat_file, 'file')
            if exist(temporary_flat_file, 'file')
                delete(temporary_flat_file)
            end
            h5create(temporary_flat_file, '/1', [cropped_width * cropped_height, n_frames_downsampled], 'Datatype', 'single');
        end
        n_pixels = cropped_width * cropped_height;

        % Compute mean of all movies
        inputMean = sum(helper.inputMean(:, 1)) / sum(helper.inputMean(:, 2));

        % De-mean and flatten movies
        for i_chunk = 1:n_chunks
            if helper.last_preprocessed_chunk_make_flat >= i_chunk
                continue
            end
            first_frame = (i_chunk - 1) * chunk_size + 1;
            last_frame = (i_chunk - 1) * chunk_size + chunk_size;
            last_frame = min(last_frame, sum(n_frames_downsampled));

            LOGGER.info(['\tReading chunk ', num2str(i_chunk), '/', num2str(n_chunks)])
            movie = loadMovie(preprocessed_movie_filename, first_frame, last_frame);
            nFrames = size(movie, 3);
            
            % Crop movie
            % movie = movie(N:S, W:E, :);

            % Subtract grand mean
            movie = movie - inputMean;
            
            % Reshape movie into [space x time] matrix
            movie = reshape(movie, n_pixels, nFrames);

            % Subtract mean of each frame
            mean_M = mean(movie, 1);
            movie = bsxfun(@minus, movie, mean_M);

            % Write data to disk
            h5write(temporary_flat_file, '/1', movie, [1, first_frame], [n_pixels, nFrames]);

            % Update counter
            helper.last_preprocessed_chunk_make_flat = i_chunk;
            save(helper_file, 'helper', '-v7.3')
        end
        
        clear movie
    end
    
    % Signal extraction
    if p.user.large_data
        if ~helper.completed_PCAICA
            saveDir = os.path.join(local_saveDir, 'extracted');
            if ~exist(saveDir, 'dir')
                mkdir(saveDir)
            end
            resultsPCAICA_filename = os.path.join(saveDir, 'resultsPCAICA.mat');
            cellMap_filename = os.path.join(saveDir, 'cellMap.mat');

            % run PCA
            movie = loadMovie(temporary_flat_file);
            LOGGER.info([char(datetime('now')), ' Signal extraction: PCA'])

            % For some reason the output is kinda random (sometimes good, sometimes bad)
            % For this, we will flag the output and take only ones that are
            % good. Meaning having a max trace lower than a fixed value:
            % 300
            running_bad = true;
            while running_bad
                LOGGER.info([char(datetime('now')), ' Signal extraction: PCA'])

                [spatial, temporal, S] = compute_pca(movie, p.PCAICA.nPCs);
                S = diag(S);  % keep only the diagonal of S
                % clear movie

                % ICA
                LOGGER.info([char(datetime('now')), ' Signal extraction: ICA'])
                % set parameters
                mu = p.PCAICA.mu;
                num_ICs = p.PCAICA.nICs;
                term_tol = p.PCAICA.term_tol;
                max_iter = p.PCAICA.max_iter;

                % run ICA
                ica_mixed = compute_spatiotemporal_ica_input(spatial, temporal, mu);
                ica_W = compute_ica_weights(ica_mixed, num_ICs, term_tol, max_iter)';
                [filters, traces] = compute_ica_pairs(spatial, temporal, S, cropped_height, cropped_width, ica_W);
                traces = permute(traces, [2, 1]);
                % set them to 0
                traces = bsxfun(@minus, traces, median(traces,2));

                % check for traces values lower than 3000
                if max(traces(:)) < 300
                    running_bad = false;
                    fprintf('\tSuccessful PCA-ICA\n')
                else
                    fprintf('\tPCA-ICA did not converge\n')
                    p.PCAICA.nPCs = p.PCAICA.nPCs / 2;

                end
                
            end
                p.PCAICA.nPCs = 50; % default
            % put filters back into right frame and save
            % fFrame = zeros([size(zeroMask), size(filters,3)]);
            % Recalculate coordinates
            % [crop_N, crop_S, crop_W, crop_E] = getCropCoords(zeroMask);
            % fFrame(crop_N:crop_S, crop_W:crop_E, :) = filters;
            % filters = fFrame;
            cellMap = squeeze(max(filters, [], 3));

            % Increaes contrast by removing outliers
            q = quantile(cellMap(:), [.01, .99]);
            cellMap(cellMap < q(1)) = q(1);
            cellMap(cellMap > q(2)) = q(2);

            % save data
            save(resultsPCAICA_filename, 'p', 'traces', 'filters');
            save(cellMap_filename, 'cellMap', 'filters');

            helper.completed_PCAICA = true;
            save(helper_file, 'helper', '-v7.3')
        end
    end
    

%% MLint exceptions
%#ok<*TNMLP,*VIDREAD,*NASGU,*STRNU>
