function select_sq_ROI_multi_animal(p, sessionDirs, experiments, missing_only, done_preprocessing) 
%ORGANIZEDATA organizes movie files for all sessions of one subject

    global GC LOGGER

    p_filename = get_filename_of('miniscope_movie_parameters', p.subjects{1});
    animal_ID = p.subjects{1};
    
    %% STEP 2: get refFrames from all sessions for choosing ROIs
    
    % if isfield(p.user, 'position')
        fprintf('ROI positions already stored for these sessions. Would you like to overwrite, or add more? y/[n]\n')
        
           
    % else
        redo_positions = true;
    % end

    % Get a flag if the recordings are made with the Miniscope V4.
    fnames = dir(sessionDirs{1});
    dirnames = {fnames.name}';
    
    movie_names = dirnames(cellfun(@(x) endsWith(x, '.avi'), {fnames.name}'));
    % flag for Miniscope v3, if not then it is Miniscope v4
    is_v3 = ~any(ismember(dirnames, 'headOrientation.csv'));
    p.user.is_v3 = is_v3;
   

    if redo_positions
        refFrames = cell(p.nSessions, 1);
        for i_sess = 1:p.nSessions
            
%             if is_v3
                prefix = 'msCam0'; % msCam1
%             else
%                 prefix = '1';
%             end
            dir_sess =dir(sessionDirs{i_sess});
            dir_sess_names = {dir_sess.name};
            mov_files = dir_sess_names(endsWith(dir_sess_names, 'avi'));
            movPath =fullfile(sessionDirs{i_sess}, mov_files{1});    
            % movPath = os.path.join(sessionDirs{i_sess}, [prefix,'.avi']);
            
            % check if the codec is installed if not, install it
            try
                tempObj = VideoReader(movPath);
            catch
                % toogle toolbox FFPEG
                toggle_toolbox('ffmpeg-r8', 'on')
                % ffmpegsetup (M:/Software/FFmpeg/bin/ffmpeg.exe)
                disp('The .exe file is here: M:/Software/FFmpeg/bin/ffmpeg.exe')
                ffmpegsetup
            end

            movieObj = VideoReader(movPath);
            %movieObj = imread(movPath);
            % take frame number 100 as reference frame
            if strcmp(movieObj.VideoFormat, 'RGB24')
                temp_refFrame = read(movieObj,100);
                temp_refFrame = rgb2gray(temp_refFrame);
                refFrames{i_sess} = temp_refFrame;
            else
                refFrames{i_sess} = read(movieObj,100);
            end
        end

        % display the referance frames, choose ROIs such that they are
        % approximately the same for each session
        ROIsize = [310, 290];
        % TODO: this might need to be modified for MiniscopeV4 -> ROIsize =
        % [608,608]; original
        
        f = figure('units','normalized','outerposition',[0 0 1 1]);
        n_subplot_cols = 3;
        n_subplot_rows = ceil(p.nSessions / n_subplot_cols);
        for i_sess = 1:p.nSessions
            if done_preprocessing(i_sess)
                subaxis(n_subplot_rows, n_subplot_cols, i_sess, 'ml',.03, 'mr',.01, 'mt',.1, 'mb',.03, 'sv',.05, 'sh',.02);
                imagesc(refFrames{i_sess}); axis image; colormap gray;
                %imagesc(movieObj); axis image; colormap gray;
                title(['Session ' num2str(i_sess)])
                try
                    % if ~isempty(p.user.position{i_sess})
                        % h{i_sess} = imrect(gca,[p.user.position{i_sess}]);
                    % else
                        h{i_sess} = imrect(gca,[10,10,ROIsize(1),ROIsize(2)]);
                    % end
                catch
                    h{i_sess} = imrect(gca,[10,10,ROIsize(1),ROIsize(2)]);
                end
            else
                subaxis(n_subplot_rows, n_subplot_cols, i_sess, 'ml',.03, 'mr',.01, 'mt',.1, 'mb',.03, 'sv',.05, 'sh',.02);
                imagesc(refFrames{i_sess}); axis image; colormap gray;
                %imagesc(movieObj); axis image; colormap gray;
                title(['Session ' num2str(i_sess)])
                h{i_sess} = imrect(gca,[10,10,ROIsize(1),ROIsize(2)]);
            end
        end
        sgtitle({'Position the rectangles to select the best image region' ; 'Choose approx. the same area in every session' ; 'Confirm with double click on rectangles (left to right)'})
        
        % Store ROI position
        p.user.position = cell(p.nSessions, 1);
        for i_sess = 1:p.nSessions
            position = wait(h{i_sess});
            p.user.position{i_sess} = round(position);
            % Close figure
            if i_sess == p.nSessions
                close(f)
            end
        end

        % Store p to disk
        save(p_filename, 'p')
    end
    