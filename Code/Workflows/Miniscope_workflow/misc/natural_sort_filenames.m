function sorted_names = natural_sort_filenames(filenames)
    % Extract numbers from filenames
    numbers = cellfun(@(x) str2double(regexp(x, '\d+', 'match')), filenames, 'UniformOutput', false);
    numbers = cell2mat(numbers);
    
    % Sort based on the extracted numbers
    [~, idx] = sort(numbers);
    
    % Apply sorting to original filenames
    sorted_names = filenames(idx);
end

% Usage example:
% sorted_names = natural_sort_filenames(movie_names);