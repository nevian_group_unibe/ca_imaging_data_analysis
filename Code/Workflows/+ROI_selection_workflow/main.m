%% PREAMBLE
% This function launches the ROI_selection workflow, which runs a specialized
% GUI for extracting regions-of-interest (ROIs) corresponding to cells or axons.

%% MAIN
function done = main(INFO)
done = true;  % This is the output of the workflow, which is read by the launcher GUI and used to continue to the next workflow
% Get logger and general_configs
global LOGGER GC

% Unpack INFO to make all variables of this script explicit
experiments = INFO.experiments;
data_types = INFO.sessions_table.data_type(ismember(INFO.sessions_table.animal_ID, INFO.experiments));

% Log beginning of workflow
LOGGER.info('ROI selection workflow', 'decorate',true)

% Split experiments by animal
all_animal_names = natsort(experiments);
experiment_name = INFO.selected_experiment; 

for iid = 1:length(all_animal_names)
    % Get data for this animal
    animal_ID = all_animal_names{iid};
    this_data_type = data_types{iid};
    
    LOGGER.info(['Preparing data of ', animal_ID, ' (', this_data_type, '-imaging) for segmentation GUI'])
    

    % << Modify here code for automatic cell map alignment and video
    % cropping. >>

    % 
    % % Prepare data for segmentation, if they don't exist
    % Registration_workflow.prepare_data_for_segmentation(animal_ID, this_data_type, experiment_name)
    % 
    % % Launch python
    % LOGGER.info('Launching GUI_ROI_segmentation.py')
    % disp('YOU MUST SELECT ONE ROI AND SAVE IT')
    % filename_params = os.path.join(GC.temp_dir, animal_ID, [animal_ID, '_params.mat']);
    % % Load pre existing data
    % filename_remote = get_filename_of('ROI_info', animal_ID);
    % if exist(filename_remote, 'file')
    %     selection = MessageBox('Do you want to re run segmentation GUI?', 'Close Request Function', 'YES','No', 'No');
    %     if strcmp(selection, 'YES')
    %         run_in_python('GUI_ROI_segmentation', filename_params, false)
    %     end
    % else
    %     run_in_python('GUI_ROI_segmentation', filename_params, false)
    % end
    % 
    % % Process GUI's output -----------------------------------------------------
    % % Get filenames
    % filename_local = os.path.join(GC.temp_dir, animal_ID, [animal_ID, '_ROI_info.mat']);
    % 
    % % Get number of detected ROIs
    % ROIs = load_variable(filename_local, 'ROIs');
    % n_ROIs = size(ROIs, 3);
    % LOGGER.info([num2str(n_ROIs), ' ROIs detected in dataset ''', animal_ID, ''''])
    % % Update database
    % LOGGER.trace('Updating database')
    % SQL_database.update('experiments', 'n_ROIs',n_ROIs, animal_ID,'animal_ID')
    % 
    % % Ask user to copy file to server
    % if exist(filename_remote, 'file')
    %     DEFAULT = 'Yes, overwrite';
    % else
    %     DEFAULT = 'Yes';
    % end
    % selection = MessageBox('Copy ROI info to server?', 'Close Request Function', DEFAULT,'No', 'No');
    % if strcmp(selection, DEFAULT)
    %     LOGGER.info('Copying ROI info to server ...')
    %     copyfile(filename_local, filename_remote)
    %     LOGGER.info('done', 'append',true)
    % end
    DEFAULT = 'YES';
    % Extract signal with tforms (So far, it's going to re-run the signal extraction based on the new tforms (skipping the python segmentation GUI))
    METADATA = SQL_database.read_table_where('sessions', {}, animal_ID, 'animal_ID', 'return_all_sessions',true);
    experiments_info = uniqueCellRows(METADATA{:, {'date', 'experiment'}}, 'rows');


    % Align cell maps
    runAlignment(animal_ID, experiments_info)
    % Concatenate and normalize movies
    extractSignalsJoint(animal_ID, experiments_info)

    % 1. evaluate if the cell maps are well aligned
    % load maps aligned
    aligned_maps_filename = fullfile(GC.registered_images, animal_ID, [animal_ID,'_alignedCellMaps.mat']);
    mapsAligned = load_variable(aligned_maps_filename, 'mapsAligned');
    tforms = load_variable(aligned_maps_filename, 'tforms');
    
    % Check the cellmaps
    [~, isApproved] = cellmap_viewer(mapsAligned);

    if ~isApproved
        % adjust manually
        %check if .mat file exists in the temp folder
        tform_save_file = fullfile(GC.temp_dir, animal_ID, 'session_transforms.mat');
        
        % load or create new tforms
        if exist(tform_save_file, 'file')
            LOGGER.info(' ## Loading tforms from already done correction ##')
            tforms = load_variable(tform_save_file, 'transforms');
        else
            LOGGER.info(' ## Adjusting FOV manually through the APP ##')
            tforms = align_sessions_with_tforms(animal_ID, tforms);
        end
    
    end % Let's see later how we fix it


    % check if the corrected file exists
    corrected_file = fullfile(GC.temp_dir, animal_ID, [animal_ID '_joint_corrected.h5']);
    if exist(corrected_file, 'file')
        LOGGER.info(' ## Corrected file was found, skipping ##')

    else
        LOGGER.info(' ## Extracting Signal joint with new tforms ##')
        extractSignalsJoint_tforms(animal_ID, experiments_info, tforms)
        disp(' ## Signal was extracted and concatenated file is stored ##')
    end
    
    % selection = MessageBox('Do you want to continue with CNMF-e analysis?', 'Close Request Function', DEFAULT,'No', 'No');
    % if strcmp(selection, DEFAULT)
        % LOGGER.info('Adjust FOV based on manual adjustment')
        toggle_toolbox('CNMF_E', 'on')
        
        % % so far tforms are only from epi data
        % if ~strcmp(this_data_type, 'epi') 
        %     done_adjustment = adjust_FOV_from_registration(animal_ID,this_data_type);
        % else
        %     done_adjustment = adjust_FOV_from_registration_tforms(animal_ID,this_data_type, tforms);
        % end

        % if done_adjustment
            selection = MessageBox('Do you want to plot the FOV?', 'Close Request Function', DEFAULT,'No', 'No');
            if strcmp(selection, 'YES')
                LOGGER.info('Checking FOV adjustment')
                % test_FOV_correction(animal_ID,this_data_type )
                test_FOV_correction_tforms(animal_ID)
                happyhere = MessageBox('Do you like the FOV?', 'Close Request Function', DEFAULT,'No', 'No');
               
                % fix stupid logic later
                if strcmp(happyhere, 'YES')
                    happyhere = true;
                else
                    happyhere = false;
                end

                while ~happyhere
                    tforms = align_sessions_with_tforms(animal_ID, tforms);
                    extractSignalsJoint_tforms(animal_ID, experiments_info, tforms)
                    test_FOV_correction_tforms(animal_ID)
                    happyhere = MessageBox('Do you like the FOV?', 'Close Request Function', DEFAULT,'No', 'No');
                    if strcmp(happyhere, 'YES')
                        happyhere = true;
                    else
                        happyhere = false;
                    end
                end
            end
        end
        
        LOGGER.info('About to perform CNFM-e')
        done_CNMFe = Run_CNMFe(animal_ID,this_data_type);
        toggle_toolbox('CNMF_E', 'off')
        
        % upload the roi info (tforms) to the server
        tforms_server = fullfile(GC.data_root_path, GC.segmentation_path,  [animal_ID, '_tforms.mat']);
        save(tforms_server, 'tforms');
    % end

end


%% MLint exceptions
%#ok<*AGROW,*NASGU,*STRNU>
