
classdef SessionOverlayTool < handle
    properties
        Figure
        MainAxis
        SessionImages
        CurrentSession = 1
        NumSessions
        Transparency = 0.5
        CurrentTransform
        SavedTransforms
        XSlider
        YSlider
        RotationSlider
        TransparencySlider
        SessionSelector
        TransformText
        ContrastSlider
        BrightnessSlider
    end

    methods
        function app = SessionOverlayTool(session_means)
            app.SessionImages = session_means;
            app.NumSessions = length(session_means);
            app.SavedTransforms = cell(1, app.NumSessions);
            app.SavedTransforms{1} = rigidtform2d(0, [0 0]); % Reference session

            % Initialize transforms for other sessions
            for i = 2:app.NumSessions
                app.SavedTransforms{i} = rigidtform2d(0, [0 0]);
            end

            % Create figure
            app.Figure = figure('Name', 'Session Alignment Tool', ...
                'Position', [100 100 1200 700], ...
                'NumberTitle', 'off', ...
                'MenuBar', 'none', ...
                'CloseRequestFcn', @(src,event) app.closeFigure());

            % Create main display axis
            app.MainAxis = axes('Parent', app.Figure, ...
                'Position', [0.1 0.2 0.8 0.7]);
            title('Session Overlay View');

            % Create controls
            app.createControls();

            % Initial display
            app.updateDisplay();
        end

        function createControls(app)
            % Create a control panel at the bottom
            controlPanel = uipanel('Parent', app.Figure, ...
                'Position', [0.05 0.02 0.9 0.15], ... % Make panel taller and wider
                'Title', 'Controls');

            % Session selector
            uicontrol('Parent', controlPanel, ...
                'Style', 'text', ...
                'String', 'Active Session:', ...
                'Position', [10 70 80 20]);

            app.SessionSelector = uicontrol('Parent', controlPanel, ...
                'Style', 'popup', ...
                'String', arrayfun(@(x) sprintf('Session %d', x), 1:app.NumSessions, 'UniformOutput', false), ...
                'Position', [90 70 100 20], ...
                'Callback', @(src,event) app.sessionChanged());

            % Translation sliders
            app.XSlider = uicontrol('Parent', controlPanel, ...
                'Style', 'slider', ...
                'Min', -50, 'Max', 50, ...
                'Value', 0, ...
                'Position', [210 70 150 20], ...
                'Callback', @(src,event) app.transformChanged());

            app.YSlider = uicontrol('Parent', controlPanel, ...
                'Style', 'slider', ...
                'Min', -50, 'Max', 50, ...
                'Value', 0, ...
                'Position', [210 40 150 20], ...
                'Callback', @(src,event) app.transformChanged());

            % Labels for translation sliders
            uicontrol('Parent', controlPanel, ...
                'Style', 'text', ...
                'String', 'X:', ...
                'Position', [190 70 20 20]);

            uicontrol('Parent', controlPanel, ...
                'Style', 'text', ...
                'String', 'Y:', ...
                'Position', [190 40 20 20]);

            % Rotation slider
            uicontrol('Parent', controlPanel, ...
                'Style', 'text', ...
                'String', 'Rotation:', ...
                'Position', [370 70 50 20]);

            app.RotationSlider = uicontrol('Parent', controlPanel, ...
                'Style', 'slider', ...
                'Min', -180, 'Max', 180, ...
                'Value', 0, ...
                'Position', [420 70 150 20], ...
                'Callback', @(src,event) app.transformChanged());

            % Transparency slider
            uicontrol('Parent', controlPanel, ...
                'Style', 'text', ...
                'String', 'Transparency:', ...
                'Position', [370 40 70 20]);

            app.TransparencySlider = uicontrol('Parent', controlPanel, ...
                'Style', 'slider', ...
                'Min', 0, 'Max', 1, ...
                'Value', app.Transparency, ...
                'Position', [420 40 150 20], ...
                'Callback', @(src,event) app.transparencyChanged());

            % Transform display
            app.TransformText = uicontrol('Parent', controlPanel, ...
                'Style', 'text', ...
                'String', 'Transform: [0,0], 0°', ...
                'Position', [580 70 200 20]);

            % Add Contrast and Brightness controls
            uicontrol('Parent', controlPanel, ...
                'Style', 'text', ...
                'String', 'Contrast:', ...
                'Position', [580 40 50 20]);

            app.ContrastSlider = uicontrol('Parent', controlPanel, ...
                'Style', 'slider', ...
                'Min', 0.1, 'Max', 3, ...
                'Value', 1, ...
                'Position', [630 40 100 20], ...
                'Callback', @(src,event) app.adjustImage());

            % Brightness slider
            uicontrol('Parent', controlPanel, ...
                'Style', 'text', ...
                'String', 'Brightness:', ...
                'Position', [580 10 60 20]);

            app.BrightnessSlider = uicontrol('Parent', controlPanel, ...
                'Style', 'slider', ...
                'Min', -1, 'Max', 1, ...
                'Value', 0, ...
                'Position', [630 10 100 20], ...
                'Callback', @(src,event) app.adjustImage());

            % Save button
            uicontrol('Parent', controlPanel, ...
                'Style', 'pushbutton', ...
                'String', 'Save Transforms', ...
                'Position', [780 40 100 30], ...
                'Callback', @(src,event) app.saveTransforms());
        end

        function sessionChanged(app)
            app.CurrentSession = app.SessionSelector.Value;
            % Update sliders to reflect current transform
            if app.CurrentSession > 1  % Only for non-reference sessions
                tform = app.SavedTransforms{app.CurrentSession};
                % Set slider values from the existing transform
                app.XSlider.Value = tform.Translation(1);
                app.YSlider.Value = tform.Translation(2);
                app.RotationSlider.Value = tform.RotationAngle * 180/pi;

                % Update transform text
                app.TransformText.String = sprintf('Transform: [%.4f,%.4f], %.4f°', ...
                    tform.Translation(1), tform.Translation(2), ...
                    tform.RotationAngle * 180/pi);
            end
            app.updateDisplay();
        end

        function transformChanged(app)
            if app.CurrentSession > 1 % Don't transform reference session
                % Get current values from sliders
                x_trans = app.XSlider.Value;
                y_trans = app.YSlider.Value;
                rot_deg = app.RotationSlider.Value;

                % Convert rotation to radians
                rot_rad = rot_deg * pi/180;

                % Create rigidtform2d directly
                app.SavedTransforms{app.CurrentSession} = rigidtform2d(rot_rad, [x_trans, y_trans]);

                % Update transform display text
                app.TransformText.String = sprintf('Transform: [%.4f,%.4f], %.4f°', ...
                    x_trans, y_trans, rot_deg);

                app.updateDisplay();
            end
        end

        function transparencyChanged(app)
            app.Transparency = app.TransparencySlider.Value;
            app.updateDisplay();
        end

        % function updateDisplay(app)
        %     cla(app.MainAxis);
        %     hold(app.MainAxis, 'on');
        %
        %     % Display reference image
        %     imagesc(app.MainAxis, app.SessionImages{1});
        %     colormap(app.MainAxis, 'gray');
        %
        %     % Overlay current session with transparency
        %     if app.CurrentSession > 1
        %         moving_img = imwarp(app.SessionImages{app.CurrentSession}, ...
        %             app.SavedTransforms{app.CurrentSession});
        %
        %         % Create alpha data for transparency
        %         h = imagesc(app.MainAxis, moving_img);
        %         set(h, 'AlphaData', app.Transparency);
        %     end
        %
        %     axis(app.MainAxis, 'image');
        %     hold(app.MainAxis, 'off');
        % end

        function saveTransforms(app)
            app.SavedTransforms = app.SavedTransforms;
            [filename, pathname] = uiputfile('session_transforms.mat', 'Save Transforms');
            if filename ~= 0
                transforms = app.SavedTransforms;
                save(fullfile(pathname, filename), 'transforms');
                msgbox('Transforms saved successfully!');
            end
        end

        function adjustImage(app)
            app.updateDisplay();
        end

        function updateDisplay(app)
            cla(app.MainAxis);
            hold(app.MainAxis, 'on');

            % Get contrast and brightness values
            contrast = app.ContrastSlider.Value;
            brightness = app.BrightnessSlider.Value;

            % Display reference image with adjustments
            ref_img = app.SessionImages{1};
            ref_img = app.adjustImageIntensity(ref_img, contrast, brightness);
            imagesc(app.MainAxis, ref_img);
            colormap(app.MainAxis, 'gray');

            % Overlay current session with transparency if it's not the reference
            if app.CurrentSession > 1
                % Apply transform directly to original image
                moving_img = app.SessionImages{app.CurrentSession};
                tform = app.SavedTransforms{app.CurrentSession};

                % Create output view to match reference image
                outputView = imref2d(size(ref_img));

                % Apply transform to original (untransformed) image
                warped_img = imwarp(moving_img, tform, 'OutputView', outputView);

                % Apply contrast and brightness adjustments
                warped_img = app.adjustImageIntensity(warped_img, contrast, brightness);

                % Display warped image with transparency
                h = imagesc(app.MainAxis, warped_img);
                set(h, 'AlphaData', app.Transparency);
            end

            axis(app.MainAxis, 'image');
            hold(app.MainAxis, 'off');
        end

        % Helper function for contrast and brightness adjustment
        function adjusted_img = adjustImageIntensity(app, img, contrast, brightness)
            % Normalize image to [0,1] range
            img_min = min(img(:));
            img_max = max(img(:));
            img_norm = (img - img_min) / (img_max - img_min);

            % Apply contrast
            img_contrast = (img_norm - 0.5) * contrast + 0.5;

            % Apply brightness
            img_bright = img_contrast + brightness;

            % Clip values to [0,1]
            adjusted_img = max(0, min(1, img_bright));

            % Scale back to original range
            adjusted_img = adjusted_img * (img_max - img_min) + img_min;
        end

        function loadTransform(app, tform)
            % For rigidtform2d, we can directly access the properties
            rot_deg = tform.RotationAngle * 180/pi;
            x_trans = tform.Translation(1);
            y_trans = tform.Translation(2);

            % Update sliders
            app.XSlider.Value = x_trans;
            app.YSlider.Value = y_trans;
            app.RotationSlider.Value = rot_deg;

            % Update transform display
            app.TransformText.String = sprintf('Transform: [%.4f,%.4f], %.4f°', ...
                x_trans, y_trans, rot_deg);
        end

        function closeFigure(app)
            delete(app.Figure);
        end
    end
end