function aligned_transforms = align_multiple_sessions_miniscope(animal_ID)
% ALIGN_MULTIPLE_SESSIONS Load and align multiple imaging sessions
% Input:
%   animal_ID - String with animal identifier
% Output:
%   aligned_transforms - Cell array of transforms for each session

% Define the base directory
base_dir = fullfile('K:', 'Ca_imaging_pain', '2_motion_corrected_movies', animal_ID);

% Find all h5 files in the directory
h5_files = dir(fullfile(base_dir, '*.h5'));

if isempty(h5_files)
    error('No H5 files found in directory: %s', base_dir);
end

% Initialize cell array for mean images
session_means = cell(1, length(h5_files));

% Process each session
fprintf('Processing %d sessions...\n', length(h5_files));

for sess_idx = 1:length(h5_files)
    current_file = fullfile(h5_files(sess_idx).folder, h5_files(sess_idx).name);
    fprintf('Processing session %d/%d: %s\n', sess_idx, length(h5_files), h5_files(sess_idx).name);
    
    % Get file info
    h5_info = h5info(current_file);
    image_dims = h5_info.Datasets(1).Dataspace.Size(1:2);
    num_frames = h5_info.Datasets(1).Dataspace.Size(3);
    
    % Initialize mean accumulator
    accumulated_mean = zeros(image_dims);
    frame_chunk_size = 5000;
    total_frames = 0;
    
    % Process in chunks
    for chunk_start = 1:frame_chunk_size:num_frames
        chunk_end = min(chunk_start + frame_chunk_size - 1, num_frames);
        chunk_frames = chunk_end - chunk_start + 1;
        
        % Read chunk
        try
            chunk_data = h5read(current_file, '/1', ...
                [1, 1, chunk_start], ...
                [image_dims(1), image_dims(2), chunk_frames]);
            
            % Calculate mean of chunk and accumulate
            chunk_mean = mean(double(chunk_data), 3);
            accumulated_mean = accumulated_mean + (chunk_mean * chunk_frames);
            total_frames = total_frames + chunk_frames;
            
            fprintf('  Processed frames %d to %d\n', chunk_start, chunk_end);
        catch ME
            warning('Error reading chunk in file %s: %s', h5_files(sess_idx).name, ME.message);
            continue;
        end
    end
    
    % Calculate final mean for this session
    session_means{sess_idx} = accumulated_mean / total_frames;
end

% Launch the modified alignment tool
app = MultiSessionAlignmentTool(session_means);