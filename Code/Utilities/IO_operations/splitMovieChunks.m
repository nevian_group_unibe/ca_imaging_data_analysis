function success = splitMovieChunks(videoPath, movDir, chunkSize)
    if nargin < 3
        chunkSize = 1000;
    end
    try
        video_to_take = fullfile(movDir, videoPath);
        % Read video
        v = VideoReader(video_to_take);
        [~, baseName, ext] = fileparts(video_to_take);
    
        % Calculate chunks
        totalFrames = v.NumFrames;
        numChunks = ceil(totalFrames/chunkSize);
        chunksCreated = 0;
    
        % Process each chunk
        for chunk = 1:numChunks
            disp(['Processing chunk ' num2str(chunk) '/' num2str(numChunks)])
            % Calculate frame range for this chunk
            startFrame = (chunk-1)*chunkSize + 1;
            endFrame = min(chunk*chunkSize, totalFrames);
    
            % Create video writer for this chunk
            chunkName = sprintf('%s%d',  baseName(1:end-1), chunk);
            outputPath = fullfile(movDir, [chunkName, '.avi']);
            writer = VideoWriter(outputPath, 'Motion JPEG AVI');
            writer.FrameRate = v.FrameRate;
            open(writer);
    
            % Set video position to start frame
            v.CurrentTime = (startFrame-1)/v.FrameRate;
    
            % Write frames
            for frame = startFrame:endFrame
                img = readFrame(v);
                writeVideo(writer, img);
                % Display progress within chunk
                if mod(frame-startFrame+1, 100) == 0
                    fprintf('Processing frames: %d/%d\r', frame-startFrame+1, endFrame-startFrame+1);
                end
            end
            fprintf('\n'); % New line after chunk completion
            close(writer);
            chunksCreated = chunksCreated + 1;
            disp(['Completed chunk ' num2str(chunk) '/' num2str(numChunks)])
    
        end
        % Verify all chunks were created
        if chunksCreated == numChunks
            delete(videoPath);
            success = true;
        end
    
    catch ME
        warning(ME.identifier, '%s', ME.message);
        success = false;
    end
end