function aligned_transforms = align_sessions_with_tforms(animal_ID, original_tforms)
% ALIGN_SESSIONS_WITH_TFORMS Load sessions, apply existing tforms, and allow manual adjustment
% Inputs:
%   animal_ID - String with animal identifier
%   original_tforms - Cell array of original transforms to start with

% Define the base directory
base_dir = fullfile('K:', 'Ca_imaging_pain', '2_motion_corrected_movies', animal_ID);

% Find all h5 files in the directory
h5_files = dir(fullfile(base_dir, '*.h5'));

if isempty(h5_files)
    error('No H5 files found in directory: %s', base_dir);
end

fprintf('Preprering data');

% Calculate mean images for each session
session_means = cell(1, length(h5_files));
fprintf('Processing %d sessions...\n', length(h5_files));

for i = 1:length(h5_files)
    current_file = fullfile(h5_files(i).folder, h5_files(i).name);
    fprintf('Processing session %d/%d: %s\n', i, length(h5_files), h5_files(i).name);
    
    % Get file info
    info = h5info(current_file);
    image_dims = info.Datasets(1).Dataspace.Size(1:2);
    num_frames = info.Datasets(1).Dataspace.Size(3);
    
    % Process in chunks
    chunk_size = 5000;
    accumulated_mean = zeros(image_dims);
    total_frames = 0;
    
    for chunk_start = 1:chunk_size:num_frames
        chunk_end = min(chunk_start + chunk_size - 1, num_frames);
        chunk_frames = chunk_end - chunk_start + 1;
        
        % Read chunk
        chunk_data = h5read(current_file, '/1', ...
            [1, 1, chunk_start], ...
            [image_dims(1), image_dims(2), chunk_frames]);
        
        accumulated_mean = accumulated_mean + sum(double(chunk_data), 3);
        total_frames = total_frames + chunk_frames;
    end
    
    session_means{i} = accumulated_mean / total_frames;
end

% Apply original transforms to mean images
transformed_means = cell(size(session_means));
transformed_means{1} = session_means{1};  % Reference session stays the same

for i = 2:length(session_means)
    if i <= length(original_tforms)
        outputView = imref2d(size(session_means{1}));
        transformed_means{i} = imwarp(session_means{i}, original_tforms{i}, ...
            'OutputView', outputView);
    else
        transformed_means{i} = session_means{i};
    end
end

% Launch the alignment tool with untransformed means
app = SessionOverlayTool(session_means);  % Pass original, untransformed means

% If original transforms exist, initialize them in the tool
if ~isempty(original_tforms)
    app.SavedTransforms = original_tforms;
    % Force an update of the current session to set initial slider values
    app.CurrentSession = 2;  % Start with first non-reference session
    app.sessionChanged();    % This will update sliders to match original tform
end

waitfor(app.Figure);

if isprop(app, 'SavedTransforms')
    aligned_transforms = app.SavedTransforms;
else
    aligned_transforms = [];
end

end