function success = splitMovieChunksFFmpeg(videoPath, movDir)
    success = false;
    try
        videoPath = fullfile(movDir, videoPath);

        % Create the output pattern with proper formatting
        outputPattern = fullfile(movDir, 'msCam_%d.avi');
        
        % Modified command:
        % 1. Added -segment_time instead of segment_frames
        % 2. Added -break_non_keyframes 1 to force splitting
        % 3. Added -segment_format avi to ensure proper AVI formatting
        cmd = sprintf(['ffmpeg -i "%s" -c copy -f segment -segment_format avi ', ...
            '-reset_timestamps 1 -segment_time 50 -break_non_keyframes 1 ', ...
            '-segment_list_size 0 "%s"'], ...
            videoPath, outputPattern);
            
        % Execute the command
        [status, cmdout] = system(cmd);
        
        if status == 0
            % Verify that segments were created before deleting source
            segmentPattern = fullfile(movDir, 'msCam_*.avi');
            segments = dir(segmentPattern);
            
            if ~isempty(segments)
                % Calculate expected number of segments
                videoInfo = dir(videoPath);
                if videoInfo.bytes > 0
                    fprintf('Created %d segments successfully\n', length(segments));
                    delete(videoPath);
                    success = true;
                else
                    warning('Source video appears to be empty');
                end
            else
                warning('No segments were created despite successful FFmpeg execution');
            end
        else
            warning('FFmpeg error: %s', cmdout);
        end
    catch ME
        warning(ME.identifier, '%s', ME.message);
    end

end